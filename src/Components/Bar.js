import React from 'react'
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import {FiEdit2} from 'react-icons/fi'
import {AiFillFolderOpen} from 'react-icons/ai'
import {MdDeleteForever} from 'react-icons/md'
import {MdOutlineApartment} from 'react-icons/md'
import {HiLocationMarker} from 'react-icons/hi'

export default function Bar() {
  return (
    <div>
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
  <div class="container-fluid">
  <h2 className='title' >Customer Service Representative <br/>
  <HiLocationMarker style={{fontSize:20,color:'green'}}/>Senegal,Dakar</h2>
  <br/>
  <div>

  </div>
  
    <form class="d-flex">
      
    <button type="button" class="btn btn-info" style={{background:'white' ,color:'green', borderRadius:10,borderColor:'black', fontSize:20,margin:5}}><AiFillFolderOpen/>open</button>
    <button type="button" class="btn btn-info" style={{background:'white' ,color:'black', borderRadius:30,borderColor:'black', fontSize:20,margin:5}}><MdOutlineApartment/></button>
    <button type="button" class="btn btn-info" style={{background:'white' ,color:'black', borderRadius:25,borderColor:'black', fontSize:20,margin:5}}><FiEdit2/></button>
    <button type="button" class="btn btn-info" style={{background:'white' ,color:'black', borderRadius:25,borderColor:'black', fontSize:20,margin:5}}><MdDeleteForever/></button>
    </form>
  </div>
 
</nav>

       
      
    <Navbar collapseOnSelect expand="lg" bg="light" variant="light" class="main-header navbar navbar-expand navbar-white navbar-light" >
       
      <Container>
        <Navbar.Brand href="#home">SUMMARY</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="#features">APPLICANT</Nav.Link>
            <Nav.Link href="#pricing">JOE BOARD</Nav.Link>
            <Nav.Link href="#pricing">INTERVIENTS</Nav.Link>
            <Nav.Link href="#pricing">ACTIVITY</Nav.Link>
            <Nav.Link href="#pricing">RECADECAST</Nav.Link>
            <Nav.Link href="#pricing">NOTIFICATION</Nav.Link>
          
          </Nav>
          
        </Navbar.Collapse>
      </Container>
    </Navbar>

    </div>
  )
}
