import React from "react";
import './App.css'
import Sidebar from "./Components/Sidebar";
import Nav from "./Components/Nav";
import Bar from "./Components/Bar";
import Page from "./Components/Page";
import Footer from "./Components/Footer";


function App() {
  return (
    <div className="App">
     <Nav/>
     <Bar/>
     <Sidebar/>
    <Page/>
    <hr/>
    <Footer/>
    </div>
  );
}

export default App;
