import React, { PureComponent } from 'react';
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

export default class Example extends PureComponent {
  

  state = {
    data: [
      {
        name: 'L',
        uv: 500,
        pv: 2400,
        amt: 2400,
      },
      {
        name: 'M',
        uv: 1800,
        pv: 1398,
        amt: 2210,
      },
      {
        name: 'M',
        uv: 2500,
        pv: 9800,
        amt: 2290,
      },
      {
        name: 'J',
        uv: 3200,
        pv: 3908,
        amt: 2000,
      },
      {
        name: 'V',
        uv: 1000,
        pv: 4800,
        amt: 2181,
      },
      {
        name: 'S',
        uv: 600,
        pv: 3800,
        amt: 2500,
      },
      {
        name: 'D',
        uv: 300,
        pv: 4300,
        amt: 2100,
      },

      {
        name: 'V',
        uv: 1000,
        pv: 4800,
        amt: 2181,
      },
      {
        name: 'S',
        uv: 600,
        pv: 3800,
        amt: 2500,
      },
      {
        name: 'D',
        uv: 300,
        pv: 4300,
        amt: 2100,
      },
      {
        name: 'M',
        uv: 1800,
        pv: 1398,
        amt: 2210,
      },
      {
        name: 'M',
        uv: 2500,
        pv: 9800,
        amt: 2290,
      },
    ],
    activeIndex: 0,
  };

  handleClick = (data, index) => {
    this.setState({
      activeIndex: index,
    });
  };

  render() {
    const { activeIndex, data } = this.state;
    const activeItem = data[activeIndex];

    return (
      <div style={{ width: '100%' }} >
       
        <ResponsiveContainer width="100%" height={300}>
          <BarChart width={150} height={200} data={data}>
            <Bar dataKey="uv" onClick={this.handleClick}>
              {data.map((entry, index) => (
                <Cell cursor="pointer" fill={index === activeIndex ? 'mediumseagreen' : 'mediumseagreen'} key={`cell-${index}`} />
              ))}
            </Bar>
          </BarChart>
        </ResponsiveContainer>
        <a href="#" class="btn btn-primary" style={{background:'aliceblue',borderRadius:15,color:'black'}}>APPLICANTS/DAY</a>
      
      </div>
    );
  }
}





